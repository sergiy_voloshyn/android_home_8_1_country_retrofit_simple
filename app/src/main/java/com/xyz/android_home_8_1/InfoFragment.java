package com.xyz.android_home_8_1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InfoFragment extends Fragment {
    List<Country> countryInfo;

    @BindView(R.id.name_country)
    TextView countryName;

    @BindView(R.id.capital)
    TextView countryCapital;

    @BindView(R.id.region)
    TextView countryRegion;

    @BindView(R.id.numericCode)
    TextView countryNumericCode;

    @BindView(R.id.currencies)
    TextView countryCurrencies;

    @BindView(R.id.languages)
    TextView countryLanguages;


    @BindView(R.id.imageView)
    ImageView countryImageView;


    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);

        if (countryInfo != null) {
            setData(countryInfo);
        }

        return view;
    }

    public void updateData(List<Country> country) {
        this.countryInfo = country;
        if (isResumed()) {
            setData(country);
        }
    }


    public void setData(List<Country> country) {

        if (country.size() == 1) {

            if (countryName != null) countryName.setText("Country name: " + country.get(0).name);
            if (countryCapital != null)
                countryCapital.setText("Country capital: " + country.get(0).capital);
            if (countryRegion != null)
                countryRegion.setText("Country region: " + country.get(0).region);
            if (countryNumericCode != null)
                countryNumericCode.setText("Country numeric code: " + country.get(0).numericCode);

            if (countryCurrencies != null) {
                countryCurrencies.setText("Country currencies: ");
                for (int i = 0; i < country.get(0).currencies.length; i++) {
                    countryCurrencies.append("\n" + (i + 1) + "-  " + country.get(0).currencies[i].name.toString());
                }
            }

            if (countryLanguages != null) {
                countryLanguages.setText("Country languages: ");
                for (int i = 0; i < country.get(0).languages.length; i++) {
                    countryLanguages.append("\n" + (i + 1) + "-  " + country.get(0).languages[i].name.toString());
                }
            }

            if (countryImageView != null) {
                //Picasso.with(getContext()).load(country.get(0).flagImageURL).into(countryImageView);
            }
        }

    }
}