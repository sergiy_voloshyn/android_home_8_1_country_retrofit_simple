package com.xyz.android_home_8_1;

import android.icu.text.IDNA;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/*
1. Используя сервис https://restcountries.eu/ создать приложение
"Каталог стран". Приложение должно уметь показывать список частей
света (regions) частей частей света (sub regions) и, собственно,
стран. Должна быть возможность просмотра информации по стране
(столица, население, языки и пр. 5-7 пунктов).
1.1* Добавить возможность фильтрации результатов (стран) по названию,
языку и столицам. Должно выглядеть так: пользователь в текстовое поле
вводит текст, если этот текст содержится в названии страны или ее
столицы - она остается в списке, если нет - исчезает.
 */

public class MainActivity extends AppCompatActivity implements ElementClickListener {


    ListFragment regionsFragment;
    ListFragment subregionsFragment;
    ListFragment countriesFragment;
    InfoFragment countryInfoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {


            Retrofit.getRegions(new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {

                    regionsFragment = ListFragment.newInstance();
                    HashSet<String> newSet = new HashSet<String>();
                    for (int i = 0; i < countries.size(); i++) {
                        String temp = countries.get(i).region.toString();
                        if (temp != "") newSet.add(temp);
                    }

                    regionsFragment.setList(new ArrayList<String>(newSet));

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.base_container, regionsFragment)
                            .commit();
                }

                @Override
                public void failure(RetrofitError error) {
                    //Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }

    }


    @Override
    public void onItemClick(String item) {
        int check = getSupportFragmentManager().getBackStackEntryCount();

        if (check == 0) {

            Retrofit.getSubregionByName(item, new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {


                    subregionsFragment = ListFragment.newInstance();

                    HashSet<String> newSet = new HashSet<String>();
                    for (int i = 0; i < countries.size(); i++) {
                        String temp = countries.get(i).subregion;
                        if (temp != "") newSet.add(temp);
                    }

                    subregionsFragment.setList(new ArrayList<String>(newSet));

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.base_container, subregionsFragment)
                            .addToBackStack(null)
                            .commit();
                }

                @Override
                public void failure(RetrofitError error) {

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }

        if (check == 1) {

            Retrofit.getCountriesByName(item, new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {


                    countriesFragment = ListFragment.newInstance();

                    HashSet<String> newSet = new HashSet<String>();
                    for (int i = 0; i < countries.size(); i++) {
                        String temp = countries.get(i).name;
                        if (temp != "") newSet.add(temp);
                    }

                    countriesFragment.setList(new ArrayList<String>(newSet));

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.base_container, countriesFragment)
                            .addToBackStack(null)
                            .commit();
                }

                @Override
                public void failure(RetrofitError error) {

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }

        if (check == 2) {

            Retrofit.getCountryInfo(item, new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {


                    countryInfoFragment = InfoFragment.newInstance();

                    countryInfoFragment.updateData(countries);


                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.base_container, countryInfoFragment)
                            .addToBackStack(null)
                            .commit();

                }


                @Override
                public void failure(RetrofitError error) {

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }
    }


}

