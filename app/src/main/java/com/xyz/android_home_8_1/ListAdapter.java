package com.xyz.android_home_8_1;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.RecursiveTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.xyz.android_home_8_1.R.id.number;

/**
 * Created by user on 28.01.2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    List<String> list;
    Context context;
    OnListClick onListClick;


    interface OnListClick {
        void onItemClick(int position);
    }


    public void setOnListClick(OnListClick onListClick) {
        this.onListClick = onListClick;

    }

    public String getItem(int position) {
        return list.get(position);
    }


    public ListAdapter(Context context, List<String> list) {
        this.list = list;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.fragment_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.updateData(list.get(position), position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();


    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.number)
        TextView numberPosition;

        @BindView(R.id.name)
        TextView name;


        String data;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(String data, int position) {
            this.data = data;

            numberPosition.setText(Integer.toString(position + 1) + ".");
            name.setText(data);

        }

        @OnClick(R.id.root)
        void onItemClick() {
            if (onListClick != null) {
                onListClick.onItemClick(getAdapterPosition());
            }

        }
    }

}






