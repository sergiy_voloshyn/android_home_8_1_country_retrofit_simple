package com.xyz.android_home_8_1;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView listView;

    List<String> dataList;
    ListAdapter listAdapter;

    ElementClickListener elementClickListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ElementClickListener) {
            elementClickListener = (ElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }
    }

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    public void setList(List<String> dataList) {

        this.dataList = dataList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        listAdapter = new ListAdapter(getContext(), dataList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(listAdapter);

        listAdapter.setOnListClick(new ListAdapter.OnListClick() {
            @Override
            public void onItemClick(int position) {
                String contact = listAdapter.getItem(position);
                elementClickListener.onItemClick(contact);
            }
        });
        return view;

    }

}
