package com.xyz.android_home_8_1;

/**
 * Created by user on 27.01.2018.
 */

public class Country {
    public String name;
    public String region;
    public String subregion;
    public String capital;
    public String numericCode;
    public currency[] currencies;
    public language[] languages;
    public String flagImageURL;


    class currency {
        public String code;
        public String name;
        public String symbol;

    }

    class language {
        public String iso639_1;
        public String iso639_2;
        public String name;
        public String nativeName;


    }


}
