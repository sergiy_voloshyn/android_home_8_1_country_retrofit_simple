package com.xyz.android_home_8_1;

import java.util.List;

/**
 * Created by user on 28.01.2018.
 */

public interface ElementClickListener {
    void onItemClick(String item);

}
